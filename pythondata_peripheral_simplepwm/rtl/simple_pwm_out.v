// © 2020 - 2022 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

// Stop LiteX silently ignoring net naming / missing register errors
`default_nettype none

module simple_pwm_out_wishbone #(
		parameter BASE_CLOCK_FREQUENCY_HZ = 50000000,
		parameter REQUIRED_PWM_FREQUENCY_HZ = 25000
	)
	(
		// Wishbone slave port signals
		input wire slave_wb_cyc,
		input wire slave_wb_stb,
		input wire slave_wb_we,
		input wire [29:0] slave_wb_addr,
		input wire [31:0] slave_wb_dat_w,
		output wire [31:0] slave_wb_dat_r,
		input wire [3:0] slave_wb_sel,
		output wire slave_wb_ack,
		output wire slave_wb_err,

		input wire peripheral_reset,
		input wire peripheral_clock,

		input wire [31:0] slave_wb_dat_w,
		input wire pwm_clock,

		output wire pwm1_out,
		output wire pwm2_out,
		output wire pwm3_out,
		output wire pwm4_out
	);

	// Control and status registers
	wire [63:0] device_id;
	wire [31:0] device_version;
	reg [31:0] control_reg1 = 0;
	wire [31:0] status_reg1;

	// Device identifier
	assign device_id = 64'h7c5250545350574d;
	assign device_version = 32'h00010000;

	reg slave_wb_ack_reg = 0;
	reg [31:0] slave_wb_dat_r_reg = 0;

	assign slave_wb_ack = slave_wb_ack_reg;
	assign slave_wb_dat_r = slave_wb_dat_r_reg;

	parameter MMIO_TRANSFER_STATE_IDLE = 0;
	parameter MMIO_TRANSFER_STATE_TR01 = 1;

	reg [31:0] mmio_buffer_address_reg = 0;
	reg [7:0] mmio_transfer_state = 0;
	reg [31:0] mmio_cfg_space_tx_buffer = 0;
	reg [31:0] mmio_cfg_space_rx_buffer = 0;

	// Wishbone connector
	always @(posedge peripheral_clock) begin
		if (peripheral_reset) begin
			// Reset Wishbone interface / control state machine
			slave_wb_ack_reg <= 0;

			mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
		end else begin
			case (mmio_transfer_state)
				MMIO_TRANSFER_STATE_IDLE: begin
					// Compute effective address
					mmio_buffer_address_reg[31:2] = slave_wb_addr;
					case (slave_wb_sel)
						4'b0001: mmio_buffer_address_reg[1:0] = 0;
						4'b0010: mmio_buffer_address_reg[1:0] = 1;
						4'b0100: mmio_buffer_address_reg[1:0] = 2;
						4'b1000: mmio_buffer_address_reg[1:0] = 3;
						4'b1111: mmio_buffer_address_reg[1:0] = 0;
						default: mmio_buffer_address_reg[1:0] = 0;
					endcase

					if (slave_wb_cyc && slave_wb_stb) begin
						// Configuration register space access
						// Single clock pulse signals in deasserted state...process incoming request!
						if (!slave_wb_we) begin
							// Read requested
							case ({mmio_buffer_address_reg[7:2], 2'b00})
								0: mmio_cfg_space_tx_buffer = device_id[63:32];
								4: mmio_cfg_space_tx_buffer = device_id[31:0];
								8: mmio_cfg_space_tx_buffer = device_version;
								12: mmio_cfg_space_tx_buffer = control_reg1;
								16: mmio_cfg_space_tx_buffer = status_reg1;
								default: mmio_cfg_space_tx_buffer = 0;
							endcase

							// Endian swap
							slave_wb_dat_r_reg[31:24] <= mmio_cfg_space_tx_buffer[7:0];
							slave_wb_dat_r_reg[23:16] <= mmio_cfg_space_tx_buffer[15:8];
							slave_wb_dat_r_reg[15:8] <= mmio_cfg_space_tx_buffer[23:16];
							slave_wb_dat_r_reg[7:0] <= mmio_cfg_space_tx_buffer[31:24];

							// Signal transfer complete
							slave_wb_ack_reg <= 1;

							mmio_transfer_state <= MMIO_TRANSFER_STATE_TR01;
						end else begin
							// Write requested
							case ({mmio_buffer_address_reg[7:2], 2'b00})
								// Device ID / version registers cannot be written, don't even try...
								12: mmio_cfg_space_rx_buffer = control_reg1;
								// Status registers cannot be written, don't even try...
								default: mmio_cfg_space_rx_buffer = 0;
							endcase

							if (slave_wb_sel[0]) begin
								mmio_cfg_space_rx_buffer[7:0] = slave_wb_dat_w[31:24];
							end
							if (slave_wb_sel[1]) begin
								mmio_cfg_space_rx_buffer[15:8] = slave_wb_dat_w[23:16];
							end
							if (slave_wb_sel[2]) begin
								mmio_cfg_space_rx_buffer[23:16] = slave_wb_dat_w[15:8];
							end
							if (slave_wb_sel[3]) begin
								mmio_cfg_space_rx_buffer[31:24] = slave_wb_dat_w[7:0];
							end

							case ({mmio_buffer_address_reg[7:2], 2'b00})
								12: control_reg1 <= mmio_cfg_space_rx_buffer;
							endcase

							// Signal transfer complete
							slave_wb_ack_reg <= 1;

							mmio_transfer_state <= MMIO_TRANSFER_STATE_TR01;
						end
					end
				end
				MMIO_TRANSFER_STATE_TR01: begin
					// Cycle complete
					slave_wb_ack_reg <= 0;
					mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
				end
				default: begin
					// Should never reach this state
					mmio_transfer_state <= MMIO_TRANSFER_STATE_IDLE;
				end
			endcase
		end
	end

	// Instantiate simple PWM modules and connect signals
	simple_pwm_driver #(
		.BASE_CLOCK_FREQUENCY_HZ(BASE_CLOCK_FREQUENCY_HZ),
		.REQUIRED_PWM_FREQUENCY_HZ(REQUIRED_PWM_FREQUENCY_HZ),
		.PWM_CTL_BITS(8)
	) simple_pwm_driver_1(
		.base_clock(pwm_clock),
		.pwm_set(control_reg1[7:0]),
		.pwm_out(pwm1_out)
	);
	simple_pwm_driver #(
		.BASE_CLOCK_FREQUENCY_HZ(BASE_CLOCK_FREQUENCY_HZ),
		.REQUIRED_PWM_FREQUENCY_HZ(REQUIRED_PWM_FREQUENCY_HZ),
		.PWM_CTL_BITS(8),
	) simple_pwm_driver_2(
		.base_clock(pwm_clock),
		.pwm_set(control_reg1[15:8]),
		.pwm_out(pwm2_out)
	);
	simple_pwm_driver #(
		.BASE_CLOCK_FREQUENCY_HZ(BASE_CLOCK_FREQUENCY_HZ),
		.REQUIRED_PWM_FREQUENCY_HZ(REQUIRED_PWM_FREQUENCY_HZ),
		.PWM_CTL_BITS(8)
	) simple_pwm_driver_3(
		.base_clock(pwm_clock),
		.pwm_set(control_reg1[23:16]),
		.pwm_out(pwm3_out)
	);
	simple_pwm_driver #(
		.BASE_CLOCK_FREQUENCY_HZ(BASE_CLOCK_FREQUENCY_HZ),
		.REQUIRED_PWM_FREQUENCY_HZ(REQUIRED_PWM_FREQUENCY_HZ),
		.PWM_CTL_BITS(8)
	) simple_pwm_driver_4(
		.base_clock(pwm_clock),
		.pwm_set(control_reg1[31:24]),
		.pwm_out(pwm4_out)
	);
endmodule
