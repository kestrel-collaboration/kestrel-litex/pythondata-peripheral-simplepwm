// © 2020 - 2024 Raptor Engineering, LLC
//
// Released under the terms of the LGPL v3+
// See the LICENSE file for full details

// Stop LiteX silently ignoring net naming / missing register errors
`default_nettype none

module simple_pwm_driver #(
		parameter PWM_CTL_BITS = 8
	)
	(
		input wire base_clock,
		input [31:0] phy_cfg,

		input wire [PWM_CTL_BITS-1:0] pwm_set,
		output wire pwm_out
	);

	reg pwm_out_reg = 0;
	assign pwm_out = pwm_out_reg;

	reg [PWM_CTL_BITS-1:0] pwm_counter = 0;

	reg [23:0] pwm_clock_div_counter = 0;

	always @(posedge base_clock) begin
		if (pwm_clock_div_counter > phy_cfg[23:0]) begin
			if (pwm_counter < pwm_set) begin
				pwm_out_reg <= 1;
			end else begin
				pwm_out_reg <= 0;
			end

			pwm_counter <= pwm_counter + 1;
			pwm_clock_div_counter <= 0;
		end else begin
			pwm_clock_div_counter <= pwm_clock_div_counter + 1;
		end
	end
endmodule

module simple_pwm_tach #(
		parameter BASE_CLOCK_FREQUENCY_HZ = 100000000,
		parameter TACHOMETER_SAMPLE_RATE_HZ = 5,
		parameter TACHOMETER_MAX_FAN_RPM = 30000,
		parameter TACH_COUNT_BITS = 16
	)
	(
		input wire base_clock,

		output wire [TACH_COUNT_BITS-1:0] tach_count,
		input wire tach_in
	);

	reg [TACH_COUNT_BITS-1:0] tach_count_reg = 0;
	assign tach_count = tach_count_reg;

	reg [TACH_COUNT_BITS-1:0] tach_counter = 0;
	reg tach_in_prev = 0;
	reg tach_in_reg = 0;

	reg [31:0] tach_clock_div_counter = 0;
	reg [31:0] tach_sample_clock_div_counter = 0;

	always @(posedge base_clock) begin
		if (tach_clock_div_counter > (BASE_CLOCK_FREQUENCY_HZ / TACHOMETER_SAMPLE_RATE_HZ)) begin
			tach_count_reg <= tach_counter;
			tach_counter <= 0;

			tach_clock_div_counter <= 0;
			tach_sample_clock_div_counter <= 0;
		end else begin
			if (tach_sample_clock_div_counter > (BASE_CLOCK_FREQUENCY_HZ / (TACHOMETER_MAX_FAN_RPM / 60))) begin
				if ((tach_in_prev == 1) && (tach_in_reg == 0)) begin
					tach_counter <= tach_counter + 1;
				end

				tach_in_prev <= tach_in_reg;
				tach_in_reg <= tach_in;

				tach_sample_clock_div_counter <= 0;
			end else begin
				tach_sample_clock_div_counter <= tach_sample_clock_div_counter + 1;
			end

			tach_clock_div_counter <= tach_clock_div_counter + 1;
		end
	end
endmodule
